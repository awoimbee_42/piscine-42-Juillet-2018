# Ma piscine à 42 Paris en Juillet 2018
Il s'agissait de ma première piscine, je n'avais pas préparé les sujets chez moi ni quoi que ce soit :
 - Day00: 100/100
 - Day01: 100/100
 - Day02: 80/100 (dernier exercice mal compris)
 - Day03: 100/100
 - Day04: 100/100
 - Day05: 70/100 (ex18, 20, 21, 22 KO)
 - Day06: 100/100
 - Day07: 80/100 (ex07 KO)
 - Day08: 100/100
 - Day09: N/A (journée 24h, pas faite)
 - Day10: 100/100
 - Day11: 100/100
 - Day12: -42/100 (fonction interdite dans ex02)
 - Day13: 70/100 (derniers exercices pas faits)
 - EvalExpr: 100/100
 - Sastantua: 0/100 (pas de rendu)
 - Rush00: 0/100 (retard + mauvaise gestion d'erreur)
 - Rush01: 88/100 (cas special non géré mais excellentes performances)
 - BSQ: Pas inscrit
 - Exam00: 80/100
 - Exam01: 75/100
 - Exam02: 80/100
 - Exam Final: 58/100
